# Patches from archlinux
# https://projects.archlinux.org/svntogit/packages.git/tree/trunk?h=packages/libmad

patch_tree('amd64-64bit.patch')
patch_tree('frame_length.patch')
patch_tree('optimize.patch')

remove('aclocal.m4')
remove('Makefile.in')

touch('NEWS')
touch('AUTHORS')
touch('ChangeLog')

autoreconf()

build(static_lib=True)
collect_licenses('COPYRIGHT COPYING CREDITS')
