build(libs='-lws2_32')
collect_programs('mpc')
collect_licenses('AUTHORS COPYING')
collect_docs('NEWS README')

if cond('variant-stable'):
    collect_version()

if cond('variant-trunk'):
    collect_version(include_rev=True)
