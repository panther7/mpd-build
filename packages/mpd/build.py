options = '--enable-openal'
libs = '-static-libgcc -static-libstdc++ -lz'

if cond('target-windows'):
    libs += ' -lole32'
    options += ' --disable-alsa'

build(options=options, libs=libs)
collect_programs('mpd')
collect_licenses('AUTHORS COPYING')
collect_docs('NEWS README')
collect_file('doc/mpdconf.example', 'conf/example-mpd.conf')

collect_version(include_rev=True) 
